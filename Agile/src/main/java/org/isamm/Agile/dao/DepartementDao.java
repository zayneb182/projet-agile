package org.isamm.Agile.dao;

import org.isamm.Agile.entities.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
@RepositoryRestResource
public interface DepartementDao extends JpaRepository<Departement, Long>{

}
